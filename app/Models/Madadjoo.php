<?php

namespace App\Models;

use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\Model;
use Baloot\EloquentHelper;

class Madadjoo extends Model
{
    use EloquentHelper;
    const USER_TYPE = 4;// مددجو
    protected $table = "users";

    public function needs()
    {
        return $this->hasMany(Needs::class);
    }

    public function getGenderMadadjooAttribute()
    {
        return $this["gender"] == 1 ? "زن" : "مرد";
    }

    public function setBirthdayAttribute($value)
    {
        $jalali = Verta::parseFormat("Y/m/d", $value)->addDays(2);
        $this->attributes["birthday"] = $jalali->formatGregorian("Y-m-d 00:00:00");
    }


    protected $casts = [
        "birthday" => "date"
    ];

    public function scopeMadadjoo($query)
    {
        return $query->whereType(4);
    }


}

