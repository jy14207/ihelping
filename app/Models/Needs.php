<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Needs extends Model
{
    public function madadjoo(){
        return $this->belongsTo(Madadjoo::class);
    }
}
