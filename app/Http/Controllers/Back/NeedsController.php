<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\models\Needs;
use Illuminate\Http\Request;

class NeedsController extends Controller
{
    public function index()
    {
        return view("back.needs.list");
    }

    public function create()
    {
        return view("back.needs.create");
    }

    public function store(Request $request)
    {
        //
    }


    public function show(Needs $needs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Needs  $needs
     * @return \Illuminate\Http\Response
     */
    public function edit(Needs $needs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Needs  $needs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Needs $needs)
    {
        //
    }
    public function categoryForm(){
        return view("back.needs.categories");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Needs  $needs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Needs $needs)
    {
        //
    }
}
