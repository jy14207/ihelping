<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Charity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CharityController extends Controller
{
    public function index()
    {
        $charities = Charity::paginate(10);
        return view("back.superadmin.charity",compact('charities'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           "name"=>"required",
            "tell"=>"iran_phone",
        ]);
        $charity = new Charity();
        $charity->name = $request->name;
        $charity->tell = $request->tell;
        $charity->address = $request->address;
        $charity->save();
        return self::redirectWithSuccess(route('superadmin.charities.index'),'ثبت با موفقیت انجام شد');
        //
    }

    public function show(Charity $charity)
    {
        //
    }

    public function edit(Charity $charity)
    {
        //
    }

    public function update(Request $request, Charity $charity)
    {
        //
    }

    public function destroy(Charity $charity)
    {
        //
    }

    public function createAdminCharityUser(){

        $charities = Charity::get();
        $collection = collect($charities);
        $charitiesKey = $collection->mapWithKeys(function ($item){
           return [$item['id']=>$item['name']];
        });
        $users = User::get()->where("type","2");
        return view('back.superadmin.charityAdminUser',compact('charitiesKey','users'));
    }
    public function storeAdminCharityUser(Request $request){
        $this->validate($request, [
            "name" => "required",
            "national_code" => "required|unique:users|min:10|max:10",
            "mobile" => 'iran_mobile',
            "email" => "required|unique:users",
            "user_name" => "required|unique:users",
            'password' => "required|min:6|confirmed",
        ]);
        $user = new User();
        $user->email = $request->email;
        $user->charity_id = $request->charity_id;
        $user->type = config('app.CHARITY_ADMIN_USER');
        $user->name = $request->name;
        $user->national_code = $request->national_code;
        $user->user_name = $request->user_name;
        $user->password = Hash::make($request->password);
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->save();
        $users = User::get()->where("type","2");
        return self::redirectWithSuccess(route('superadmin.charities.createAdminCharityUser'), 'ثبت با موفقیت انجام شد',compact('users'));
    }
}
