<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\models\Madadjoo;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Config;
use Hekmatinasser\Verta;

class MadadjooController extends Controller
{
    public function index()
    {
        $charity_id = Auth::user()->charity_id;
        $madadjoos = Madadjoo::where("type","4")->where('charity_id',$charity_id)->paginate(10);
        return view('back.madadjoos.all', compact('madadjoos'));
    }

    public function create()
    {
        return view("back.madadjoos.create");
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "national_code" => "required|unique:users|min:10|max:10",
            "mobile" => 'iran_mobile',
            "address" => "required",
        ]);
        $madadjoo = new Madadjoo;
        $madadjoo->charity_id = Auth::user()->charity_id;
        $madadjoo->city_id = $request->city_id;
        $madadjoo->type = Madadjoo::USER_TYPE;
        $madadjoo->name = $request->name;
        $madadjoo->national_code = $request->national_code;
        $madadjoo->family_members = $request->family_members;
        $madadjoo->gender = $request->gender;
        $madadjoo->birthday = $request->birthday;
//        dd(Verta\Verta::getGregorian(1394,10,4));
        $madadjoo->mobile = $request->mobile;
        $madadjoo->tell = $request->tell;
        $madadjoo->address = $request->address;
        $madadjoo->save();
        return self::redirectWithSuccess(route('admin.madadjoos.create'), 'ثبت با موفقیت انجام شد');
    }


    public function show(Madadjoo $madadjoo)
    {
        $needs=$madadjoo->needs()->get();
        return view('back.madadjoos.show', compact('needs','madadjoo'));

    }


    public function edit(Madadjoo $madadjoo)
    {
        //
    }


    public function update(Request $request, Madadjoo $madadjoo)
    {
        //
    }


    public function destroy(Madadjoo $madadjoo)
    {
        //
    }

    public function select2SearchMadadjoo(Request $request)
    {
        $data = [];
            $search = $request->q;
            $data =Madadjoo::where('name','LIKE',"%$search%")
                ->orwhere('national_code','LIKE',"%$search%")
                ->madadjoo()
                ->limit(20)
                ->get();
        return response()->json($data);
    }
}
