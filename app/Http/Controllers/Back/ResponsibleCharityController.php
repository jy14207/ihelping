<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\models\ResponsibleCharity;
use Illuminate\Http\Request;

class ResponsibleCharityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\ResponsibleCharity  $responsibleCharity
     * @return \Illuminate\Http\Response
     */
    public function show(ResponsibleCharity $responsibleCharity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\ResponsibleCharity  $responsibleCharity
     * @return \Illuminate\Http\Response
     */
    public function edit(ResponsibleCharity $responsibleCharity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\ResponsibleCharity  $responsibleCharity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResponsibleCharity $responsibleCharity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\ResponsibleCharity  $responsibleCharity
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResponsibleCharity $responsibleCharity)
    {
        //
    }
}
