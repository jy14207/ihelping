<div class="clearfix">
    <nav aria-label="breadcrumb" class="float-right">
        <ol class="breadcrumb">
            @foreach($items as $title=>$link)
                @if($title == "current")
                    @continue
                @endif
                <li class="breadcrumb-item"><a href="{{ $link }}"> @lang('messages.'.$title)</a></li>
            @endforeach
            <li class="breadcrumb-item active" aria-current="page">{{ $items["current"] ?? "" }}</li>
        </ol>
    </nav>
    <div class="float-left">{{ $slot }}</div>
</div>
