@extends('back.layout.app')
@section('title','داشبورد سوپر ادمین')
@section('content')
    <x-success></x-success>
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('superadmin.charities.store')}}"
                  method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <strong>ثبت مرکز نیکوکاری</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{
                                    Html::text("name")
                                    ->value(old("name"))
                                    ->label("نام مرکز نیکوکاری :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("tell")
                                    ->value(old("tell"))
                                    ->label("تلفن:")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("address")
                                    ->value(old("address"))
                                    ->label("آدرس:")
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>لیست مراکز نیکوکاری ثبت شده : </strong>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ردیف</th>
                            <th>نام مرکز نیکوکاری</th>
                            <th>تلفن</th>
                            <th>آدرس</th>
                            <th>وضعیت</th>
                            <th>حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($charities as $charity)
                            <tr>
                                <td></td>
                                <td>{{$loop->iteration }}</td>
                                <td>{{$charity->name}}</td>
                                <td>{{$charity->tell}}</td>
                                <td>{{$charity->address}}</td>
                                <td>فعال/غیرفعال</td>
                                <td>حذف</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> خروجی اکسل</a>
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> چاپ</a>

                    </div>
                </div>
            </div>
        </div>

    </div>

@stop