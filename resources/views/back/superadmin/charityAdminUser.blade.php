@extends('back.layout.app')
@section('title','داشبورد سوپر ادمین')
@section('content')
    <x-success></x-success>
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('superadmin.charities.storeAdminCharityUser')}}"
                  method="post" enctype="multipart/form-data">
                {{ Html::hidden()->name('user_type')->value(config('app.CHARITY_ADMIN_USER')) }}

                @csrf
                <div class="card">
                    <div class="card-header">
                        <strong>تعریف کاربر مدیر مرکز نیکوکاری</strong>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-4">
                                {{
                                    Html::select("charity_id")
                                        ->label("نام مرکز نیکوکاری:")
                                        ->value(old("charity_id"))
                                        ->description("")
                                        ->select_options($charitiesKey)
                                    }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("name")
                                    ->value(old("name"))
                                    ->label("نام و نام خانوادگی کاربر مدیر :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("national_code")
                                    ->value(old("national_code"))
                                    ->label("کد ملی :")
                                }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {{
                                    Html::text("user_name")
                                    ->value(old("user_name"))
                                    ->label("نام کاربری :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("email")
                                    ->value(old("email"))
                                    ->label("ایمیل :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("password")
                                    ->value(old("password"))
                                    ->label("گذرواژه :")
                                }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {{
                                    Html::text("password_confirmation")
                                    ->value(old("password_confirmation"))
                                    ->label("تکرار گذرواژه :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("mobile")
                                    ->value(old("mobile"))
                                    ->label("شماره همراه :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("address")
                                    ->value(old("address"))
                                    ->label("آدرس :")
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>لیست کاربران مدیر مرکز : </strong>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ردیف</th>
                            <th>نام مرکز نیکوکاری</th>
                            <th>نام و نام خانوادگی کاربر مدیر</th>
                            <th>کد ملی</th>
                            <th>نام کاربری</th>
                            <th>ایمیل</th>
                            <th>شماره همراه</th>
                            <th>وضعیت</th>
                            <th>حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td></td>
                                <td>{{$loop->iteration }}</td>
                                <td>{{$user->charity_id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->national_code}}</td>
                                <td>{{$user->user_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->mobile}}</td>
                                <td>فعال/غیرفعال</td>
                                <td>حذف</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> خروجی اکسل</a>
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> چاپ</a>

                    </div>
                </div>
            </div>
        </div>

    </div>

@stop