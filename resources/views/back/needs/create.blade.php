                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               @extends('back.layout.app')
@section('title','اضافه کردن نیاز')
@section('content')
    <x-success></x-success>
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('admin.categories.store')}}"
                  method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <strong>ثبت نیاز</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="user_select" class="col-form-label text-md-right">نام یا کد ملی مددجو :</label>
                                {{--<select class="form-control users-search" id="select-user" name="user_id">
                                </select>--}}
                                <select class="select2-search-madadjoo form-control" name="itemName"></select>
                                <small>مددجوی مورد نظر خود را انتخاب کنید</small>
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("title")
                                    ->value(old("title"))
                                    ->label("عنوان نیاز :")
                                }}
                            </div>
                            <div class="col-md-4">
                                {{
                                    Html::text("price")
                                    ->value(old("price"))
                                    ->label("مبلغ مورد نیاز :")
                                }}
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{
                                    Html::textarea("short_text")
                                    ->value(old("short_text"))
                                    ->rows("3")
                                    ->label("توضیح کوتاه در مورد نیاز :")
                                }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>شرح حال کامل مددجو :</label>
                                <textarea name="body" id="body" value="sdsdsadsd" rows="50" cols="40" class="form-control tinymce-editor"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>--}}
    <script type="text/javascript">

    </script>
@stop