@extends('back.layout.app')
@section('title','اطلاعات مددجو')
@section('content')
    <x-success></x-success>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>مشخصات مددجو</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-4 col-xs-offset-3 col-lg-4 col-xl-2">
                            <img src="/images/avatar-placeholder.png" alt="" class="w-100 rounded-circle">
                        </div>
                        <div class="col-md-10 col-lg-8 col-xl-10">
                            <div class="text-success h4"><i class="fa fa-user"></i> {{ $madadjoo->name }} </div>
                            {{Html::info()->value($madadjoo->national_code)->label("- کد ملی ") }}
                            {{Html::info()->value($madadjoo->family_members)->label("- تعداد اعضای خانواده (نفر) ") }}
                            {{Html::info()->value($madadjoo->gender_madadjoo)->label("- جنسیت ") }}
                            {{Html::info()->value($madadjoo->birthday_fa_f)->label("- تاریخ تولد ") }}
                            {{Html::info()->value($madadjoo->mobile)->label("- شماره همراه ") }}
                            {{Html::info()->value($madadjoo->tell)->label("- تلفن ") }}
                            {{Html::info()->value($madadjoo->address)->label("- آدرس ") }}

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> ویرایش مشخصات </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>نیاز های ثبت شده برای مددجو</strong>
                </div>
                <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>ردیف</th>
                                <th>عنوان</th>
                                <th>اعتبار مورد نیاز</th>
                                <th>وضعیت</th>
                                <th>رزرو</th>
                                <th>رزرو</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($needs as $need)
                                <tr>
                                    <td></td>
                                    <td>{{$loop->iteration }}</td>
                                    <td>{{$need->title}}</td>
                                    <td>{{$need->price}}</td>
                                    <td>{{$need->status}}</td>
                                    <td>گزارش واریز</td>
                                    <td>ویرایش</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> دکمه رزرو</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>کمک های جمع آوری شده برای نیاز : </strong>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ردیف</th>
                            <th>نام و نام خانوادگی خیر</th>
                            <th>مبلغ کمک شده</th>
                            <th>تاریخ</th>
                            <th>ساعت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($needs as $need)
                            <tr>
                                <td></td>
                                <td>{{$loop->iteration }}</td>
                                <td>{{$need->title}}</td>
                                <td>{{$need->price}}</td>
                                <td>__/__/____</td>
                                <td>..:..</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> خروجی اکسل</a>
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> چاپ</a>

                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection