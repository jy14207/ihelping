@extends('back.layout.app')
@section('title','لیست مددجو')
@section('content')
    <x-success></x-success>
    {{$madadjoos->render()}}
    <div class="card">
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>نام و نام خانوادگی</th>
                    <th>کد ملی</th>
                    <th>تعداد اعضای خانوار</th>
                    <th>شماره همراه</th>
                    <th>آدرس</th>
                </tr>
                </thead>
                <tbody>
                @foreach($madadjoos as $madadjoo)
                    <tr>
                        <td></td>
                        <td><a href="{{route('admin.madadjoos.show',$madadjoo)}}" >{{ $madadjoo->name }}</a></td>
                        <td class="text-left">{{ $madadjoo->national_code }}</td>
                        <td>{{ $madadjoo->family_members }}</td>
                        <td>{{ $madadjoo->mobile }}</td>
                        <td>{{ $madadjoo->address }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection