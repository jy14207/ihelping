@extends('back.layout.app')
@section('title','اضافه کردن مددجو')
@section('content')
    <x-success></x-success>
    <form action="{{route('admin.madadjoos.store')}}"
          method="post" enctype="multipart/form-data">
        @csrf
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            {{
                                Html::text("name")
                                ->value(old("name"))
                                ->label("نام و نام خانوادگی :")
                            }}
                        </div>
                        <div class="col-md-4">
                            {{
                                Html::text("national_code")
                                ->value(old("national_code"))
                                ->label("کد ملی:")
                            }}
                        </div>
                        <div class="col-md-4">
                            {{
                                Html::select("family_members")
                                    ->label("تعداد اعضای خانوار:")
                                    ->value("1")
                                    ->description("")
                                    ->select_options([
                                    "1"=>"1",
                                    "2"=>"2",
                                    "3"=>"3",
                                    "4"=>"4",
                                    "5"=>"5",
                                    "6"=>"6"])
                                }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {{
                            Html::select("gender")
                                ->label("جنسیت:")
                                ->value("1")
                                ->description("")
                                ->select_options([
                                "1"=>"زن",
                                "2"=>"مرد"])
                            }}
                        </div>
                        <div class="col-md-4">
                            {{
                                Html::text("birthday")
                                ->value(old("birthday"))
                                ->label("تاریخ تولد:")
                            }}
                        </div>
                        <div class="col-md-4">
                            {{
                                Html::text("mobile")
                                ->value(old("mobile"))
                                ->label("تلفن همراه :")
                            }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {{
                                Html::text("tell")
                                ->value(old("tell"))
                                ->label("تلفن:")
                            }}
                        </div>
                        <div class="col-md-4">
                            {{
                               Html::select("city_id")
                                ->label("شهر:")
                                ->value("1")
                                ->description("")
                                ->select_options([
                                "1"=>"مشهد",
                                "2"=>"سایر"])
                            }}

                        </div>
                        <div class="col-md-4">
                            {{
                                Html::text("address")
                                ->value(old("address"))
                                ->label("آدرس:")
                            }}
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>

                    </div>
                </div>
                <div class="row">

                </div>
            </div>
        </div>

        </div>
    </form>
@stop