@extends('back.layout.app')
@section('title','دسته بندی نیاز ها')
@section('content')
    <x-success></x-success>
    <div class="row">
        <div class="col-md-12">
            <form action="{{route('admin.categories.store')}}"
                  method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <strong>ثبت دسته نیازمندی ها</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{
                                    Html::text("categoryname")
                                    ->value(old("categoryname"))
                                    ->label("نام دسته :")
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong>لیست دسته بندی های ثبت شده : </strong>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ردیف</th>
                            <th>نام دسته</th>
                            <th>حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td></td>
                                <td>{{$loop->iteration }}</td>
                                <td>{{$category->categoryname}}</td>
                                <td>حذف</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> خروجی اکسل</a>
                        <a class="btn btn-outline-info btn-sm" href="#"><i
                                    class="fa fa-edit"></i> چاپ</a>

                    </div>
                </div>
            </div>
        </div>

    </div>

@stop