<div class="form-group ">
    <label for="{{ $name }}"
           class=" col-form-label text-md-right">{{ $label ?? "" }}</label>
    <textarea id="{{ $name }}" name="{{ $name }}" rows="{{ $rows ?? 3}}"
              placeholder="{{ $placeholder ?? "" }}"
              class="form-control textarea {{ $errors->has($name) ? ' is-invalid' : '' }}">{{ $value ?? "" }}</textarea>
        <small>{{ $description ?? "" }}</small>
    @if ($errors->has($name))
        <span class="invalid-feedback" role="alert">
             <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
