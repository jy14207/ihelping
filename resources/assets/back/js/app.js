/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('UserStatusInQueue', require('./vue/nasim-moshaver/UserStatusInQueue').default);
Vue.component('UserPauseStatus', require('./vue/nasim-moshaver/UserPauseStatus').default);
Vue.component('CallsReport', require('./vue/nasim-moshaver/CallsReport').default);
// Vue.component('ThemeOptions', require('./vue/theme-options/options.vue').default);
// Vue.component('InstagramInsightPanel', require('./vue/insights/instagram/InstagramInsight.vue').default);
// Vue.component('SocialInfo', require('./vue/insights/SocialInfo.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 //  */
//
const app = new Vue({
    el: '.vue-app',
});


// import VueSweetalert2 from 'vue-sweetalert2';
// Vue.use(VueSweetalert2);

window.select2 = require("select2");
// $.fn.select2.defaults.set('amdBase', 'select2/');
//
$.fn.select2.defaults.set("theme", "bootstrap");
require('./contents/select2');
require('./contents/tinymce.min');




$(document).ready(function () {
    $('.confirmation').click(function (e) {
        if (!confirm("آیا مطمین هستید؟"))
            e.preventDefault();
    })
});

$(document).ready(function () {

    $("form.confirmBeforeSubmit").submit(function (e) {
        if (!confirm("آیا مطمئن هستید؟"))
            e.preventDefault();
    })

    $('.select2-search-madadjoo').select2({
        placeholder: "",
        "language": {
            "noResults": function () {
                return "نتیجه ای یافت نشد! (جستجو بر اساس نام یا کد ملی)";
            },
            "searching": function () {
                return "در حال جستجو. . .";
            },
        },
        ajax: {
            url: '/admin/select2-search-madadjoo',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    tinymce.init({
        selector: "textarea.tinymce-editor",
        height: 500,
        plugins: [
            "code advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",

        ],
        directionality : 'rtl',
        language : "fa",
        docs_language : "fa",
        toolbar: "code insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        setup: function (editor) {
            editor.on('init', function (e) {
                editor.execCommand('JustifyFull', false);
            });
            editor.on('init keydown change', function (e) {
                document.getElementById('body').innerText = editor.getContent();
            });
        }
    });
});
window.Highcharts = require('highcharts');
// Load module after Highcharts is loaded
require('highcharts/modules/exporting')(Highcharts);
