<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email',50)->unique()->nullable();
            $table->integer('charity_id')->nullable();
            $table->integer('city_id')->nullable();;
            $table->integer('type')->nullable();
            $table->string('name');
            $table->string('national_code')->nullable();
            $table->integer('family_members')->nullable();
            $table->boolean('gender')->nullable();
            $table->string('user_name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('password')->nullable();
            $table->string('mobile')->nullable();
            $table->string('tell')->nullable();
            $table->string('address')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
