<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('needs', function (Blueprint $table) {
            $table->id();
            $table->integer('charity_id');
            $table->integer('category_id');
            $table->integer('madadjoo_id');
            $table->string('title');
            $table->integer('price')->nullable();
            $table->string('slug'); // ?????
            $table->text('short_text')->nullable();
            $table->longText('body')->nullable();
            $table->dateTime('dead_line_at')->nullable();
            $table->dateTime('end_publish_at')->nullable();
            $table->integer('status');//should define const for this field
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('needs');
    }
}
