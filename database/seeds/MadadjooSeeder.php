<?php

use App\Classes\FakerProviders\PersianFaker;
use Illuminate\Database\Seeder;

class MadadjooSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create("fa_IR");
        $faker->addProvider(new PersianFaker($faker));
        $users = [];
        for ($i = 0; $i < 20; $i++) {
            $users[] = [
                'name' => $faker->name(),
                'email' => $faker->email(),
                'password' => bcrypt("123456"),
                'mobile' => "09370331680",
                'type' => 3,
            ];
        }
        \App\Models\Madadjoo::insert($users);
    }
}
