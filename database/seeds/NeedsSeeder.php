<?php

use Illuminate\Database\Seeder;

class NeedsSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create("fa_IR");
        $faker->addProvider(new \App\Classes\FakerProviders\PersianFaker($faker));
        $needs=[];
        for($i=0; $i<100; $i++){
            $needs[]=[
                "charity_id"=>'3',
                "category_id"=>rand(1,100),
                "madadjoo_id"=>rand(1,20),
                "title"=>$faker->name,
                "price"=>rand(1,10000),
                "slug"=>rand(1,10000),
                "status"=>rand(1,20),
            ];
        }

        \App\Models\Needs::insert($needs);

    }
}
