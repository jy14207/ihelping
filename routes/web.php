<?php

use Illuminate\Support\Facades\Route;

Route::get("php_info",function(){
    phpinfo();
});
Route::namespace("\App\Http\Controllers")->prefix("user")->as("user.")
    ->group(function () {
        //User routes
        Route::get("createUser", "UserController@create")->name("user.create");
        Route::get("userList", "UserController@index")->name("user.list");
    });

Route::namespace("\App\Http\Controllers\Back")->prefix("superadmin")->as("superadmin.")
    ->group(function (){
        route::resource('charities', 'CharityController');
        Route::get("createAdminCharityUser","CharityController@createAdminCharityUser")->name("charities.createAdminCharityUser");
        Route::post("storeAdminCharityUser","CharityController@storeAdminCharityUser")->name("charities.storeAdminCharityUser");
    });

Route::namespace("\App\Http\Controllers\Back")->prefix("admin")->as("admin.")
    ->group(function () {

        //User routes
        Route::get("createUser", "UserController@create")->name("user.create");
        Route::get("userList", "UserController@index")->name("user.list");

        //Dashboard routes
        Route::get("/", "DashboardController@index")->name("dashboard.index");

        //Madadjoo routes
        route::resource('madadjoos', 'MadadjooController');

        //Needs routes
        route::resource('needs', 'NeedsController');

        // Categories routes
        route::resource('categories','CategoriesController');

//        search-madadjoo-in-create-need-form
        Route::get('select2-search-madadjoo', 'MadadjooController@select2SearchMadadjoo');


    });

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'UserController@index');
/*Route::get('/madadjooform', 'MadadjooController@index');
Route::post('/storemadadjoo', 'MadadjooController@store')->name("storemadadjoo");*/